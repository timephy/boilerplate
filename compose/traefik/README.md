# Traefik

## Notes

- Uses Let's Encrypt to get SSL certificates
- Proxies Docker Containers with Labels (network "traefik")

## Setup

```bash
docker network create traefik

cd traefik
touch acme.json config.yml traefik.yml compose.yml
chmod 600 acme.json
```
