# Nginx Static

## Notes

- Serves static files in the `./root` directory
- Exposed through Traefik on domain (`docker-compose.yml > labels`)
