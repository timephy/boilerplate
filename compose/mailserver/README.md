# Mailserver

## Notes

- Hostname (`compose.yml → set hostname`)
- Uses Traefik's Let's Encrypt certificates (`compose.yml → acme.json path`)
- Directly exposes ports (is not proxied by Traefik)
- Remember to set a Reverse DNS (PTR) entry on the server that correlates the hostname with the server's IP (<https://docker-mailserver.github.io/docker-mailserver/latest/usage/#minimal-dns-setup0>)

## How to config

```bash
docker exec -it mailserver setup
docker exec -it mailserver setup email list
```
